view: call_center_events {
  sql_table_name: `call_events.call_center_events` ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }
  dimension: call_center {
    type: string
    sql: ${TABLE}.call_center ;;
  }

  dimension_group: call_timestamp {
    type: time
    timeframes: [raw, date, week, month, quarter, year]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.call_timestamp ;;
  }
  dimension: channel {
    type: string
    sql: ${TABLE}.channel ;;
  }
  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }
  dimension: customer_name {
    type: string
    sql: ${TABLE}.customer_name ;;
  }
  dimension: reason {
    type: string
    sql: ${TABLE}.reason ;;
  }
  dimension: response_time {
    type: string
    sql: ${TABLE}.response_time ;;
  }
  dimension: sentiment {
    type: string
    sql: ${TABLE}.sentiment ;;
  }
  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }
  measure: count {
    type: count
    drill_fields: [id, customer_name]
  }
  measure: call_duration_in_minutes {
  type: sum
  drill_fields: [id, customer_name]
  }
  measure: csat_score {
    type: average
    drill_fields: [id, customer_name]
  }
  measure: customer_count{
    type: count_distinct
    sql: ${customer_name} ;;
  }
}
