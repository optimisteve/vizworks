connection: "call_events"

# include all the views
include: "/views/**/*.view.lkml"

datagroup: call_events_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: call_events_default_datagroup

explore: call_center_events {}

# Place in `call_events` model
explore: +call_center_events {
    query: total_call_count {
      dimensions: [call_timestamp_date]
      measures: [count]
    }
}

# Place in `call_events` model
explore: +call_center_events {
    query: avg_csat_score {
      measures: [csat_score]
    }
}

# Place in `call_events` model
explore: +call_center_events {
  query: call_response_time {
      dimensions: [response_time]
      measures: [call_duration_in_minutes]
    }
}

# Place in `call_events` model
explore: +call_center_events {

    query: duration_by_call_center {
      dimensions: [call_center]
      measures: [call_duration_in_minutes]
    }
}

# Place in `call_events` model
explore: +call_center_events {

    query: sentiment_analysis {
      dimensions: [sentiment]
      measures: [call_duration_in_minutes, count, csat_score]
    }

}

# Place in `call_events` model
explore: +call_center_events {
      query: call_duration_metric {
      measures: [call_duration_in_minutes]
    }
}

# Place in `call_events` model
explore: +call_center_events {
    query: call_events_metric {
      measures: [count]
    }
}

# Place in `call_events` model
explore: +call_center_events {
  query: customer_count {
      measures: [customer_count]
    }
}

# Place in `call_events` model
explore: +call_center_events {

    query: top_10_cities_customer {
      dimensions: [city]
      measures: [customer_count]
    }
}

# Place in `call_events` model
explore: +call_center_events {
    query: top_10_cities_csat {
      dimensions: [city]
      measures: [csat_score]
    }
}
